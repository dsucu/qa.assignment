# API Test Automation Solution 


It is a sample API test project for https://openchargemap.org/. 
OpenChargeMap provides an API to retrieve charging location information, user comments, checkins and photos and to submit new comments/checkins. 
(https://openchargemap.org/site/develop/api#/)


## Design Considerations
- Serenity BDD is a library that makes it easier to write high quality automated acceptance tests, with powerful reporting and living documentation features. 
- RestAssured using for API calls
- Design based on user actions and questions to validate API response  

### The project structure

```Gherkin
src
  + test
    + java                          Test runners and supporting code
      + chargepoint                 Actions and questions on chargepoint domain
          ChargePointActions        User actions
          ChargePointQuestions      Validations
      + common                      Common Actions and Validations
          CommonQuestions           Validations for every domain
          CommonRequestSpec         Request Spec
      + stepdefinitions             Step definitions for features
          GetChargePointSteps
    + resources
      + features                    Feature files directory
          charge_point.feature     
      + schema                      JSON schema folder for validation
      serenity.conf                 Serenity configuration file

```
## Executing the tests
Run `mvn clean verify` from the command line.

The test results will be `target/site/serenity/index.html`

Reports can be seen in gitlab ci, under job artifacts section public/index.html. One of the direct link build artifact: https://dsucu.gitlab.io/-/qa.assignment/-/jobs/2004576310/artifacts/public/index.html 

### Additional configurations

Environments configurations are set in the `test/resources/serenity.conf` file.

Additional command line parameters can be passed for switching the application environment.
```json
$ mvn clean verify -Denvironment=default
```
