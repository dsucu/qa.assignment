package com.qa.assignment.common;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

public class CommonSpec {
  /**
   * Get Request Specification for openchargemap endpoint
   *
   * @return RequestSpecification
   */
  public static RequestSpecification ReqSpec() {
    EnvironmentVariables environmentVariables = Injectors.getInjector()
        .getInstance(EnvironmentVariables.class);

    String baseUrl = EnvironmentSpecificConfiguration.from(environmentVariables)
        .getProperty("baseurl");

    return new RequestSpecBuilder().setBaseUri(baseUrl)
        .setContentType("application/json")
        .build();
  }
}
