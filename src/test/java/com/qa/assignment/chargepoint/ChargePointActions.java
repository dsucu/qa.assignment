package com.qa.assignment.chargepoint;

import com.qa.assignment.common.CommonSpec;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class ChargePointActions {

    @Step("I call the get charge point details endpoint for charge point {0}")
    public Response getChargePointDetails(String id) {
        return SerenityRest.given().spec(CommonSpec.ReqSpec())
                .basePath("poi")
                .header("user-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36")
                .queryParams("ChargePointID", id, "key","301a4317-e1c0-482d-883b-5b5df40ed91d")
                .get().then().extract().response();
    }
}
