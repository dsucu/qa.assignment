package com.qa.assignment.chargepoint;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;

import static org.assertj.core.api.Assertions.assertThat;

public class ChargePointQuestions {

    @Step("Get address title from response")
    public String getTitle(Response chargepointDetailResp) {
        return (chargepointDetailResp.getBody().jsonPath().getString("[0].AddressInfo.Title"));
    }

    @Step("Validate address title {0} present in response")
    public void validateTitle(String title, Response lastResponse) {
        assertThat(getTitle(lastResponse)).isEqualToIgnoringCase(title);
    }

}
