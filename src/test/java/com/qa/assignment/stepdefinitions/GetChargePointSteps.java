package com.qa.assignment.stepdefinitions;

import com.qa.assignment.chargepoint.ChargePointActions;
import com.qa.assignment.chargepoint.ChargePointQuestions;
import com.qa.assignment.common.CommonQuestions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import static net.serenitybdd.rest.SerenityRest.lastResponse;


public class GetChargePointSteps {

  @Steps
  ChargePointActions chargePointActions;

  @Steps
  CommonQuestions commonQuestions;

  @Steps
  ChargePointQuestions chargePointQuestions;

  @When("I call the get charge point details endpoint for charge point \"(.*)\"")
  public void i_call_the_get_charge_point_endpoint_for_charge_point(String id) {
    chargePointActions.getChargePointDetails(id);
  }

  @Then("charge point details should be retrieved")
  public void chargepoint_details_should_be_retrieved() {
    commonQuestions.responseCodeIs(200, lastResponse());
  }

  @And("address title should be \"(.*)\"")
  public void titleShouldBe(String title) {
    chargePointQuestions.validateTitle(title, lastResponse());
  }

  @And("the schema should match with the specification defined in \"(.*)\"")
  public void the_schema_should_match_with_the_specification(String spec) {
    commonQuestions.verifyResponseSchema(lastResponse(), spec);
  }

}
