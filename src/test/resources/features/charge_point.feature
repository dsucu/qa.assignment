Feature: Get charge point details
  I should be able to get details of charge point

  Scenario: "Get charge point details" response schema should match with specification
    When I call the get charge point details endpoint for charge point "190378"
    Then charge point details should be retrieved
    And the schema should match with the specification defined in "charge_point.json"

  Scenario: Should be able to get charge point address details
    When I call the get charge point details endpoint for charge point "190378"
    And address title should be "Shell Kooimeer"